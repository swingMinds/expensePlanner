import 'package:expense_planner/models/transaction.dart';
import 'package:expense_planner/widgets/transaction_list.dart';
import 'package:flutter/material.dart';

import 'new_transaction.dart';

class UserTransactions extends StatefulWidget {
  @override
  _UserTransactionsState createState() => _UserTransactionsState();
}

class _UserTransactionsState extends State<UserTransactions> {
  final List<Transaction> _userTransaction = [
    Transaction(
      id: 't1',
      title: 'New Shoes',
      amount: 69.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't2',
      title: 'Weekly Condoms',
      amount: 49.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't3',
      title: 'Delay Cream',
      amount: 109.99,
      date: DateTime.now(),
    ),
  ];

  void _addNewTransaction(String tTitle, double tAmount) {
    final newTX = Transaction(
        id: DateTime.now().toString(),
        title: tTitle,
        amount: tAmount,
        date: DateTime.now());

    setState(() {
      _userTransaction.add(newTX);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        NewTransaction(_addNewTransaction),
        TransactionList(_userTransaction),
      ],
    );
  }
}
